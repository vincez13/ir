import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;
import org.jsoup.Jsoup;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class IR_P01 {
	public static void main(String[] args) throws Exception {	
		
		// Get file path, index path, method, query
		String sPath = args[0];
		String sIndexPath = args[1];
		String sSimMethod = args[2];
		String sQuery = args[3];
		
		// test
//		String sPath = "D:/workspace/TextData/";
//		String sIndexPath = "D:/workspace/indexTest/";
//		String sSimMethod = "OK";
//		String sQuery = "beautiful";
	    
		// Add documents' path to a list 
	    List<File> docList = listFile(sPath);
	    
	    // English analyzer with stop word and porter stem
	    EnglishAnalyzer analyzer = new EnglishAnalyzer();
	    
	    // get index directory path
	    Path indexPath = Paths.get(sIndexPath);
	    Directory index = MMapDirectory.open(indexPath);
	    // set writer config
		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		// set index writer
		IndexWriter w = new IndexWriter(index, config);
		// path list to prevent duplicate index
		ArrayList<String> pathList = new ArrayList<String>();
		try {
			// read into index first
			IndexReader r = DirectoryReader.open(index);
			for(int i=0; i < r.numDocs(); i++) {
				pathList.add(r.document(i).get("path"));
			}
			// Add documents to index directory; only check path so, it cannot update exist indexes
		    for (File doc : docList){
		    	addDoc(pathList, w, doc);
		    }
		    w.close();
		} catch (Exception e) {			
			// cannot find index directory
			// Add documents to index directory
			for (File doc : docList){
		    	addDoc(pathList, w, doc);
		    }
			w.close();
		}
	    
	    // Start searching
		// Our query string
	    String query = sQuery;
	    Query q;
	    // only display top 10 results
	    int hitsPerPage = 10; 
	    
	    // Read the created index and start searching
	    IndexReader reader = DirectoryReader.open(index);
	    IndexSearcher searcher = new IndexSearcher(reader);
	    // Select a comparison method
	    // VSM or BM25
	    if(sSimMethod.equals("VS")) {
	    	searcher.setSimilarity(new ClassicSimilarity());
	    }else if(sSimMethod.equals("OK")) {
	    	searcher.setSimilarity(new BM25Similarity());
	    }else {
	    	// Wrong input
	    	System.out.println("Wrong Method!");
	    	System.exit(0);
	    }
	    TopScoreDocCollector collector; // Results
	    ScoreDoc[] hits; // Store hit results
	    Document d; // Temporary document
	    
	    try {
	    	q = new QueryParser("body", analyzer).parse(query); // search body with already stemmed query
	    	collector = TopScoreDocCollector.create(hitsPerPage); // Collect results and limit results
	    	searcher.search(q, collector); // search using the query and collect at most hitsPerPage
	    	hits = collector.topDocs().scoreDocs; // Get results
	    	// Some outputs for the user
	    	if (hits.length == 0){
	    		System.out.println("Sorry, I can't find anything. :(");
	    	}else if (hits.length == 1){
	    		System.out.println("A document is found!");
	    	}else{
	    		System.out.println(hits.length + " documents are found!");
	    	}
	    	// Display documents to the user
		    for(int i=0; i<hits.length; ++i){
		    	int docId = hits[i].doc;
		    	float docScore = hits[i].score;
		    	d = searcher.doc(docId);
		    	System.out.println((i+1) + ". " + d.get("title") + "\n   " + d.get("path") + "\n   Score: " + Float.toString(docScore));
		    }
	    }catch (Exception e){
	    	System.out.println("Error: " + e);
	    }
	    index.close();
	    reader.close();	
	    System.out.println("Bye!");
	}

	// Get all text and HTML files from a directory
	private static List<File> listFile(String directoryName) throws Exception{
		File directory = new File(directoryName);
		List<File> resultList = new ArrayList<File>();
		File[] fileList = directory.listFiles();
		
		// Add every files ended with .txt or .html. If it is a directory, then uses a recursion to that directory.
		// We can add documents to the index directory here
		for (File file : fileList){
			if (file.isFile() && (file.getName().endsWith(".txt") || file.isFile() && file.getName().endsWith(".html"))){
				resultList.add(file); // Found a doc; add!
			}else if (file.isDirectory()){ // Skip directory
				resultList.addAll(listFile(file.getAbsolutePath())); // Found directory; recursion!
			}
		}
		return resultList;
	}
	
	// Add every files to a storage for searching
	private static void addDoc(ArrayList<String> pathList, IndexWriter w, File file) throws Exception {
		String title;
		String body = "";
		if (file.getName().endsWith(".txt")){
			title = file.getName().substring(0, file.getName().length()-4); // Get filename as a title
			byte[] encoded = Files.readAllBytes(Paths.get(file.getAbsolutePath())); // Read text in the file
			body = new String(encoded, StandardCharsets.UTF_8);
		}else{
			File html = new File(file.getAbsolutePath());
		    org.jsoup.nodes.Document htmlDoc = Jsoup.parse(html, "UTF-8"); // HTML parser
		    title = htmlDoc.title();
		    body = htmlDoc.text();
		}
		// Only add not the same path doc
		if(!pathList.contains(file.getAbsolutePath())) {
			Document doc = new Document();
			doc.add(new TextField("title", title, Field.Store.YES));
			doc.add(new TextField("body", title, Field.Store.YES)); // Add title to the body for searching
			doc.add(new TextField("body", body, Field.Store.YES));
			doc.add(new StringField("path", file.getAbsolutePath(), Field.Store.YES));
			w.addDocument(doc); // Add document to index
		}
	}
}
