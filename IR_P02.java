import java.io.FileWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class IR_P02 {
	private static ArrayList<String> pages = new ArrayList<String>();
	private static ArrayList<Integer> depths = new ArrayList<Integer>();

	public static void main(String[] args) throws Exception{
		String sURL = args[0];
		String sMaxDepth = args[1];
		String sIndexPath = args[2];
		String sQuery = args[3];

//		String sURL = "https://coreos.com/";
//		String sMaxDepth = "2";
//		String sIndexPath = "D:/workspace/indexTest/";
//		String sQuery = "Tectonic";

		// Set depth of crawler
		int maxDepth = Integer.parseInt(sMaxDepth);
		int currentDepth = 0;
		// base url
		String base_url = sURL;

		// start crawler
		crawl(base_url, currentDepth, maxDepth);

		// write urls to ./indexDirectory/pages.txt
		try {
			FileWriter fw = new FileWriter(sIndexPath + "pages.txt", false);
			for(int i = 0; i < pages.size(); i++) {
				fw.write(pages.get(i) + "\t" + String.valueOf(depths.get(i) + "\n"));
			}
			fw.close();

		}catch(Exception e) {
			System.out.println(e);
		}

		// English analyzer with stop word and porter stem
		EnglishAnalyzer analyzer = new EnglishAnalyzer();

		// get index directory path
		Path indexPath = Paths.get(sIndexPath);
		Directory index = MMapDirectory.open(indexPath);
		// set writer config
		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		// set index writer
		IndexWriter w = new IndexWriter(index, config);
		// url list to prevent duplicate index
		ArrayList<String> urlList = new ArrayList<String>();

		try {
			// read into index first
			IndexReader r = DirectoryReader.open(index);
			for(int i=0; i < r.numDocs(); i++) {
				urlList.add(r.document(i).get("url"));
			}
			// Add urls to index directory; it cannot update exist indexes
			for (String url : pages){
				addDoc(urlList, w, url);
			}
			w.close();
		} catch (Exception e) {			
			// cannot find index directory
			// Add documents to index directory
			for (String url : pages){
				addDoc(urlList, w, url);
			}
			w.close();
		}
		// Start searching
		// Our query string
		String query = sQuery;
		Query q;
		// only display top 10 results
		int hitsPerPage = 10; 

		// Read the created index and start searching
		IndexReader reader = DirectoryReader.open(index);
		IndexSearcher searcher = new IndexSearcher(reader);
		// Select a comparison method
		searcher.setSimilarity(new ClassicSimilarity());
		TopScoreDocCollector collector; // Results
		ScoreDoc[] hits; // Store hit results
		Document d; // Temporary document

		try {
			q = new QueryParser("body", analyzer).parse(query); // search body with already stemmed query
			collector = TopScoreDocCollector.create(hitsPerPage); // Collect results and limit results
			searcher.search(q, collector); // search using the query and collect at most hitsPerPage
			hits = collector.topDocs().scoreDocs; // Get results
			// Some outputs for the user
			if (hits.length == 0){
				System.out.println("Sorry, I can't find anything. :(");
			}else if (hits.length == 1){
				System.out.println("A document is found!");
			}else{
				System.out.println(hits.length + " documents are found!");
			}
			// Display documents to the user
			for(int i=0; i<hits.length; ++i){
				int docId = hits[i].doc;
				float docScore = hits[i].score;
				d = searcher.doc(docId);
				System.out.println((i+1) + ". " + d.get("title") + "\n   " + d.get("url") + "\n   Score: " + Float.toString(docScore));
			}
		}catch (Exception e){
			System.out.println("Error: " + e);
		}
		index.close();
		reader.close();	
		System.out.println("Bye!");
	}

	// crawler
	private static void crawl(String url, int currentDepth, int maxDepth) throws Exception {
		if(currentDepth <= maxDepth) { // if current depth is not greater than max depth 
			try {
				Connection connection = Jsoup.connect(url).userAgent("Mozilla").timeout(5000); // connect to the url with timeout
				org.jsoup.nodes.Document doc = connection.get(); // get the url page
				if(connection.response().contentType().contains("text/html")) { // check url format; only html is used
					url.toLowerCase(); // lowercase url
					if(url.charAt(url.length()-1) == '/') { // remove '/' in the end of the url
						url = url.substring(0, url.length()-1);
					}
					if(!pages.contains(url)) { // check duplicated url
						pages.add(url); // add the url if it is not duplicated
						depths.add(currentDepth); // add current depth
						Elements links = doc.select("a[href]"); // get all reference links
						for(Element link: links) { // crawl through each reference link
							crawl(link.attr("abs:href"), currentDepth+1, maxDepth); // recurive the process for each ref link with an increased depth
						}
					}
				}
			} catch( Exception e) { // in case we can't connect to the url
				System.out.println(e); // print error
				System.out.println(url); // print failed url
			}
		}
	}
	// Add every URLs to a storage for searching
	private static void addDoc(ArrayList<String> urlList, IndexWriter w, String url) throws Exception{
		String title;
		String body = "";
		Connection connection = Jsoup.connect(url).userAgent("Mozilla").timeout(5000);
		org.jsoup.nodes.Document urlDoc = connection.get();
		org.jsoup.nodes.Document htmlDoc = Jsoup.parse(urlDoc.html(), "UTF-8"); // HTML parser
		title = htmlDoc.title();
		body = htmlDoc.text();
		// Only add not the same path doc
		if(!urlList.contains(url)) {
			Document doc = new Document();
			doc.add(new TextField("title", title, Field.Store.YES));
			doc.add(new TextField("body", body, Field.Store.YES));
			doc.add(new StringField("url", url, Field.Store.YES));
			w.addDocument(doc); // Add document to index
		}
	}

}
